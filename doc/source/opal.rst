opal package
============

Subpackages
-----------

.. toctree::

    opal.analysis
    opal.datasets
    opal.parser
    opal.utilities
    opal.visualization

opal.config module
------------------

.. automodule:: opal.config

opal.opal module
----------------

.. automodule:: opal.opal