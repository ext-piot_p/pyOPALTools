opal.datasets package
=====================

Notebooks
---------

.. toctree::

   StatDataSet_Wrapper<StatWrapper>

opal.datasets.AmrDataset module
-------------------------------

.. automodule:: opal.datasets.AmrDataset

opal.datasets.DatasetBase module
--------------------------------

.. automodule:: opal.datasets.DatasetBase

opal.datasets.FieldDataset module
--------------------------------

.. automodule:: opal.datasets.FieldDataset

opal.datasets.GridDataset module
--------------------------------

.. automodule:: opal.datasets.GridDataset

opal.datasets.H5Dataset module
------------------------------

.. automodule:: opal.datasets.H5Dataset

opal.datasets.LBalDataset module
--------------------------------

.. automodule:: opal.datasets.LBalDataset

opal.datasets.LossDataset module
--------------------------------

.. automodule:: opal.datasets.LossDataset

opal.datasets.MemoryDataset module
----------------------------------

.. automodule:: opal.datasets.MemoryDataset

opal.datasets.OptimizerDataset module
-------------------------------------

.. automodule:: opal.datasets.OptimizerDataset

opal.datasets.PeakDataset module
--------------------------------

.. automodule:: opal.datasets.PeakDataset

opal.datasets.ProbeHistDataset module
-------------------------------------

.. automodule:: opal.datasets.ProbeHistDataset

opal.datasets.SDDSDatasetBase module
------------------------------------

.. automodule:: opal.datasets.SDDSDatasetBase

opal.datasets.SamplerDataset module
-----------------------------------

.. automodule:: opal.datasets.SamplerDataset

opal.datasets.SolverDataset module
----------------------------------

.. automodule:: opal.datasets.SolverDataset

opal.datasets.StatDataset module
--------------------------------

.. automodule:: opal.datasets.StatDataset

opal.datasets.StdOpalOutputDataset module
-----------------------------------------

.. automodule:: opal.datasets.StdOpalOutputDataset

opal.datasets.TimeDataset module
--------------------------------

.. automodule:: opal.datasets.TimeDataset

opal.datasets.TrackOrbitDataset module
--------------------------------------

.. automodule:: opal.datasets.TrackOrbitDataset

opal.datasets.filetype module
-----------------------------

.. automodule:: opal.datasets.filetype